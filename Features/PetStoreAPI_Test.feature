@PetStore_API_Tests
Feature: Petstore API Automation Tests

Scenario: Verify the name "doggie" and id "12" on the available pet list - Get Method
	Given User wants to execute getAvailabePets endpoint
	When User submits a Get request for available pet list
	Then User should get 200 status code
	And User should find the name = "doggie" and catagoryid= "12"
	
Scenario: Add a new pet with an auto generated name and status available - Post Method
Given User wants to execute addNewPet endpoint
When User submits Post request to create a new pet on the list.
Then User should get 200 status code and body response.

Scenario: Retrieve the created pet using the ID
Given User wants to execute getPetById endpoint
When User submits Get request for the given pet.
Then User should get 200 status code  and appropriate response body.

