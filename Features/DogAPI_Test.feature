@Dog_API_Tests
Feature: Dog API Automation Test

Scenario: Verify successful message when a user searches for random breeds - Get Method
	Given User wants to execute SearchRandomBreeds endpoint
	When User submits a Get request for random breeds
	Then User should get 200 status code and successful status message
	
Scenario: Verify that bulldog is on the list of all breeds - Get Method
	Given User wants to execute VerifyBulldogOnTheList endpoint
	When User submits a Get request for the list of all breeds
	Then User should get 200 status code and bulldog on the list
	
Scenario: Verify all bulldog sub-breeds and their images
	Given User wants to execute getSubBreedsForBulldogs endpoint - Get Method
	When User submits a Get request for all bulldog sub-breeds and their respective images
	Then User should get 200 status code and success status message
	
	

	
	
	