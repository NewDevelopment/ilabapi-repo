/**
 * 
 */
package Test.Runner;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


/**
 * Onke Ngcatsha
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(
		
		features = "Features",
		glue = {"Step_Definitions"},
		
		plugin = {"pretty", "html:target/cucumber", 
				"json:target/cucumber.json", 
				"com.cucumber.listener.ExtentCucumberFormatter:target/report.html"
				}
		
		)

public class TestRunner  {

}
