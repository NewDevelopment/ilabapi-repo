/**
 * 
 */
package Test.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * @author Hp 6560b
 *
 */
public class ReadConfig {
	
	Properties pro;

	public ReadConfig() {

		File src = new File("./Configurations/Config.properties");
		try {
			FileInputStream fis = new FileInputStream(src);
			pro = new Properties();
			pro.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getAllBreedBaseUrl() {

		String breedsUrl = pro.getProperty("AllBreedUrl");
		return breedsUrl;
	}
	
	public String getNewPetData() {
		
		String newPetData = pro.getProperty("requestBodyString");
		return newPetData;
	}

}
