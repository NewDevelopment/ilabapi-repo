/**
 * 
 */
package Step_Definitions;

import org.testng.asserts.SoftAssert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * Onke Ngcatsha
 *
 */
public class Test_Steps_getSubBreedsForBulldogs {
	
	SoftAssert softAssert = new SoftAssert();


	@Given("^User wants to execute getSubBreedsForBulldogs endpoint - Get Method$")
	public void user_wants_to_execute_getSubBreedsForBulldogs_endpoint_Get_Method() throws Throwable {
		RestAssured.given()
        .contentType(ContentType.JSON)
        .baseUri("https://dog.ceo/api/breed/bulldog/images");
	}

	@When("^User submits a Get request for all bulldog sub-breeds and their respective images$")
	public void user_submits_a_Get_request_for_all_bulldog_sub_breeds_and_their_respective_images() throws Throwable {
	   BDDStyleMethod.getBulldogSubBreeds();
	}

	@Then("^User should get (\\d+) status code and success status message$")
	public void user_should_get_status_code_and_success_status_message(int arg1) throws Throwable {
		
		Response resp = BDDStyleMethod.getBulldogSubBreeds();
		int statusCode = resp.getStatusCode();
		softAssert.assertEquals(statusCode, 200);
		
		String responseBody = resp.getBody().asString();
		softAssert.assertEquals(responseBody.toLowerCase().contains("success"), true);
		
		softAssert.assertAll();
		
	   
	}
}
