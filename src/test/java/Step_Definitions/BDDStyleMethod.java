
package Step_Definitions;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static org.hamcrest.Matchers.*;
import org.testng.asserts.SoftAssert;

/**
 * Onke Ngcatsha
 *
 */
public class BDDStyleMethod extends BaseClass {
	
	

	public static Response getRandomDogBreeds() {

		Response randomBreedResponse = RestAssured.when().get("https://dog.ceo/api/breeds/list/all").then().using()
				.extract().response();

		return randomBreedResponse;

	}

	public static Response getBulldogSubBreeds() {

		Response bulldogSubBreedsResponse = RestAssured.when().get("https://dog.ceo/api/breed/bulldog/images").then()
				.using().extract().response();

		return bulldogSubBreedsResponse;

	}
	
	public static Response getAllAvailablePets() {
		
		Response availablePetsResponse = RestAssured.when().get("https://petstore.swagger.io/v2/pet/findByStatus")
                .then().using().extract().response();
		
		return availablePetsResponse;
	}
	

	public static Response verifyCatagoryIdAndName() {
		
		Response availablePetsResponse = (Response) RestAssured.when().get("https://petstore.swagger.io/v2/pet/findByStatus")
                .then()
                .body("name", hasItem("doggie"))
                .body("category.id", hasValue(12));
		
		return availablePetsResponse;
	}

	
	public static Response postNewPet() {

		Response newPetResponse = RestAssured.given().contentType(ContentType.JSON)
		 .body(newPetData)
		 .when()
		 .post("https://petstore.swagger.io/v2/pet");
		
		return newPetResponse;
				

	}
	
	public static Response verifyNewPetResponse() {
		SoftAssert softAssert = new SoftAssert();
		Response resp = BDDStyleMethod.postNewPet();
		int statusCode = resp.getStatusCode();
	    softAssert.assertEquals(statusCode, 200);
		
		String respnseBody = resp.getBody().asString();
		softAssert.assertEquals(respnseBody.toLowerCase().contains("stew"), true);
		
		softAssert.assertAll();
		
		return resp;
	}
	
	public static Response getNewPetById() {
		
		Response petResponse = RestAssured.given().when().get("https://petstore.swagger.io/v2/pet/1222")
				.then().using().extract().response();
		
		return petResponse;
	}
	
	public static Response verifyCreatedPetById() {
		Response resp = (Response) RestAssured.given().when().get("https://petstore.swagger.io/v2/pet/1222")
				.then()
				.statusCode(200);
		
		return resp;
		
	}
	
	
	
	

}
