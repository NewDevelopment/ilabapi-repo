/**
 * 
 */
package Step_Definitions;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;



/**
 * @author Onke Ngcatsha
 *
 */
public class Test_Steps_createNewPet extends BaseClass {
	
	@Given("^User wants to execute addNewPet endpoint$")
	public void user_wants_to_execute_addNewPet_endpoint() throws Throwable {
		RestAssured.given()
		 .contentType(ContentType.JSON)
		 .baseUri("https://petstore.swagger.io/v2")
		 .basePath("/pet");
	}

	@When("^User submits Post request to create a new pet on the list\\.$")
	public void user_submits_Post_request_to_create_a_new_pet_on_the_list() throws Throwable {
	BDDStyleMethod.postNewPet();
	}

	@Then("^User should get (\\d+) status code and body response\\.$")
	public void user_should_get_status_code_and_body_response(int arg1) throws Throwable {
		
		BDDStyleMethod.verifyNewPetResponse();
	}
	



}
