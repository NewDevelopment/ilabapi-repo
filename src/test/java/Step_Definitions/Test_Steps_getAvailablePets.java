/**
 * 
 */
package Step_Definitions;

import org.testng.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


/**
 * Onke Ngcatsha
 *
 */
public class Test_Steps_getAvailablePets {
	
	
	
	@Given("^User wants to execute getAvailabePets endpoint$")
	public void user_wants_to_execute_getAvailabePets_endpoint() throws Throwable {
		RestAssured.given()
			.param("status", "available")
			.contentType(ContentType.JSON)
			.baseUri("https://petstore.swagger.io/v2/pet/findByStatus");
			

	}

	@When("^User submits a Get request for available pet list$")
	public void user_submits_a_Get_request_for_available_pet_list() throws Throwable {
		
		BDDStyleMethod.getAllAvailablePets();
	}

	@Then("^User should get (\\d+) status code$")
	public void user_should_get_status_code(int arg1) throws Throwable {
		
		Response resp = BDDStyleMethod.getAllAvailablePets();
		int statusCode = resp.getStatusCode();
		Assert.assertEquals(statusCode, 200);
		
	}

	@Then("^User should find the name = \"([^\"]*)\" and catagoryid= \"([^\"]*)\"$")
	public void user_should_find_the_name_and_catagoryid(String arg1, String arg2) throws Throwable {
		
	BDDStyleMethod.verifyCatagoryIdAndName();
		 
	}
	
	
	

}
