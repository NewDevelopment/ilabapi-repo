/**
 * 
 */
package Step_Definitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

/**
 * @Onke Ngcatsha
 *
 */
public class getNewPetById {
	
	@Given("^User wants to execute getPetById endpoint$")
	public void user_wants_to_execute_getPetById_endpoint() throws Throwable {
		RestAssured.given()
        .contentType(ContentType.JSON);
		
	 
	}

	@When("^User submits Get request for the given pet\\.$")
	public void user_submits_Get_request_for_the_given_pet() throws Throwable {
		
		BDDStyleMethod.getNewPetById();
	  
	}

	@Then("^User should get (\\d+) status code  and appropriate response body\\.$")
	public void user_should_get_status_code_and_appropriate_response_body(int arg1) throws Throwable {
		
		BDDStyleMethod.verifyCreatedPetById();
	 
	}

}
