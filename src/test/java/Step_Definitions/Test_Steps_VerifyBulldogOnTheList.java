/**
 * 
 */
package Step_Definitions;

import org.testng.asserts.SoftAssert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * Onke Ngcatsha
 *
 */
public class Test_Steps_VerifyBulldogOnTheList {
	
	SoftAssert softAssert = new SoftAssert();
	
	@Given("^User wants to execute VerifyBulldogOnTheList endpoint$")
	public void user_wants_to_execute_VerifyBulldogOnTheList_endpoint() throws Throwable {
		RestAssured.given()
        .contentType(ContentType.JSON)
        .baseUri("ttps://dog.ceo/api/breeds/list/all");
	}

	@When("^User submits a Get request for the list of all breeds$")
	public void user_submits_a_Get_request_for_the_list_of_all_breeds() throws Throwable {
		BDDStyleMethod.getRandomDogBreeds();

	}

	@Then("^User should get (\\d+) status code and bulldog on the list$")
	public void user_should_get_status_code_and_bulldog_on_the_list(int arg1) throws Throwable {
		
		Response resp =  BDDStyleMethod.getRandomDogBreeds();
		
		int statusCode = resp.getStatusCode();
		softAssert.assertEquals(statusCode, 200);
		
		String responseBody = resp.getBody().asString();
		softAssert.assertEquals(responseBody.toLowerCase().contains("bulldog"), true);
		
		softAssert.assertAll();
	 
	}

}
