/**
 * 
 */
package Step_Definitions;

import org.testng.asserts.SoftAssert;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * @author Onke Ngcatsha
 *
 */
public class Test_Steps_SearchRandomBreeds extends BaseClass{
	SoftAssert softAssert = new SoftAssert();
	
	@Given("^User wants to execute SearchRandomBreeds endpoint$")
	public void user_wants_to_execute_SearchRandomBreeds_endpoint() throws Throwable {
		RestAssured.given()
        .contentType(ContentType.JSON)
        .baseUri(breedsUrl);
	}

	@When("^User submits a Get request for random breeds$")
	public void user_submits_a_Get_request_for_random_breeds() throws Throwable {
		BDDStyleMethod.getRandomDogBreeds();
	}

	@Then("^User should get (\\d+) status code and successful status message$")
	public void user_should_get_status_code_and_successful_status_message(int arg1) throws Throwable {
		Response resp =  BDDStyleMethod.getRandomDogBreeds();
	
		int statusCode = resp.getStatusCode();
		softAssert.assertEquals(statusCode, 200);
		String responseBody = resp.getBody().asString();
		softAssert.assertEquals(responseBody.toLowerCase().contains("success"), true);
		
		softAssert.assertAll();
	}

	



}
